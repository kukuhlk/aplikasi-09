package kurniawan.kukuh.appx09

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context: Context): SQLiteOpenHelper(context,DB_Name,null,DB_Ver) {
    companion object{
        val DB_Name = "media"
        val DB_Ver = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val tLagu= "create table lagu(id_music text primary key, id_cover text not null, music_title text not null)"
        val insLagu= "insert into lagu(id_music,id_cover,music_title) values " +
                "('0x7f0c0000','0x7f06005f','Alan Walker - Faded')," +
                "('0x7f0c0001','0x7f060060','Alan Walker - Sing Me To Sleep')," +
                "('0x7f0c0002','0x7f060061','Alan Walker - Alone')"
        db?.execSQL(tLagu)
        db?.execSQL(insLagu)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }
}