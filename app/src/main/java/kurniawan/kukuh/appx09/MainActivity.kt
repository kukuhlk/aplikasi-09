package kurniawan.kukuh.appx09

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.media.MediaController2
import android.media.MediaPlayer
import android.media.session.MediaController
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    //Inisiasi Variabel
    var posLaguSkrg = 0
    var posVidSkrg = 0
    var handler = Handler()
    lateinit var mediaPlayer: MediaPlayer
    lateinit var mediaController: android.widget.MediaController

    lateinit var db : SQLiteDatabase
    lateinit var adapter : ListAdapter

    //Seekbar listener
    override fun onStartTrackingTouch(seekBar: SeekBar?) {
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        seekBar?.progress?.let { mediaPlayer.seekTo(it) }
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
    }

    //Button Listener
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnPlay ->{
                setSong(posLaguSkrg)
                audioPlay()
            }
            R.id.btnNext ->{
                audioNext()
            }
            R.id.btnPrev ->{
                audioPrev()
            }
            R.id.btnStop ->{
                audioStop()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mediaController = android.widget.MediaController(this)
        mediaPlayer = MediaPlayer()
        seekSong.max = 100
        seekSong.progress = 0
        seekSong.setOnSeekBarChangeListener(this)
        btnNext.setOnClickListener(this)
        btnPrev.setOnClickListener(this)
        btnStop.setOnClickListener(this)
        btnPlay.setOnClickListener(this)
        db = DBOpenHelper(this).writableDatabase
        lsLagu.setOnItemClickListener(itemClick)
    }

    override fun onStart() {
        super.onStart()
        showDataLagu()
    }

    //additional function

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        posLaguSkrg=position
        audioStop()
        setSong(position)
    }

    fun showDataLagu(){
        val cursor : Cursor = db.query("lagu", arrayOf("id_music as _id","id_cover","music_title"),null,null,null,null,"id_music asc")
        adapter = SimpleCursorAdapter(this,R.layout.item_data_lagu,cursor,
            arrayOf("_id","id_cover","music_title"), intArrayOf(R.id.txIdMusic, R.id.txIdCover, R.id.txJudulMusic),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        lsLagu.adapter = adapter
//        var jmllagu = lsLagu.adapter.count
//        Log.d("output","Jumlah lagu : $jmllagu")
    }

    fun setSong(pos: Int){
        val c: Cursor = lsLagu.adapter.getItem(pos) as Cursor
        var id_music = c.getInt(c.getColumnIndex("_id"))
        var id_cover = c.getInt(c.getColumnIndex("id_cover"))
        var judul = c.getString(c.getColumnIndex("music_title"))
//        Log.d("output","Posisi : $pos")
//        Log.d("output","ID music : $id_music")
//        Log.d("output","ID Cover : $id_cover")
//        Log.d("output","Judul Lagu : $judul")
        mediaPlayer = MediaPlayer.create(this, id_music)
        seekSong.max = mediaPlayer.duration
        txMaxTime.setText(milliSecondToString(seekSong.max))
        txCrTime.setText(milliSecondToString(mediaPlayer.currentPosition))
        seekSong.progress=mediaPlayer.currentPosition
        imV.setImageResource(id_cover)
        txJudulLagu.setText(judul)
    }

    fun milliSecondToString(ms : Int):String{
        var detik : Long = TimeUnit.MILLISECONDS.toSeconds(ms.toLong())
        var menit : Long = TimeUnit.SECONDS.toMinutes(detik)
        detik = detik % 60
        return "$menit : $detik"
    }

    fun audioPlay(){
        mediaPlayer.start()
        var updateSeekBarThread = UpdateSeekBarProgressThread()
        handler.postDelayed(updateSeekBarThread, 50)
    }

    fun audioNext(){
        if (mediaPlayer.isPlaying) mediaPlayer.stop()
        if (posLaguSkrg<(lsLagu.adapter.count-1)){
            posLaguSkrg++
        }else{
            posLaguSkrg = 0
        }
        setSong(posLaguSkrg)
        audioPlay()
    }

    fun audioPrev(){
        if (mediaPlayer.isPlaying) mediaPlayer.stop()
        if (posLaguSkrg>0){
            posLaguSkrg--
        }else{
            posLaguSkrg = lsLagu.adapter.count-1
        }
        setSong(posLaguSkrg)
        audioPlay()
    }

    fun audioStop(){
        if(mediaPlayer.isPlaying) mediaPlayer.stop()
    }

    inner class UpdateSeekBarProgressThread : Runnable{
        override fun run() {
            var currTime : Int = mediaPlayer.currentPosition
            txCrTime.setText(milliSecondToString(currTime))
            seekSong.progress = currTime
            if (currTime != mediaPlayer.duration) handler.postDelayed(this, 50)
        }
    }


}
